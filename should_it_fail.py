import json
import argparse

parser = argparse.ArgumentParser(description='Make the pipeline fail if security vulnerabilities have been detected')
parser.add_argument('--file', help='path to test results file')
args = parser.parse_args()

with open(args.file) as f:
	data = json.load(f)
	if len(data['alerts']) != 0:
		print("==========================")
		print("\nTEST RESULTS\n")
		print("Identified %d security vulnerabilities. See gitlab-ci artifacts for further details\n" % len(data['alerts']))
		print("==========================")
		exit(1)
